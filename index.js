const express = require("express");
const path = require("path");

const app = express();

const port = 8000;

app.get("/", (req,res) =>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/task-51.10/index.html"))
})

app.listen(3000, function() {
    console.log("Server is running on port " + 3000);
});